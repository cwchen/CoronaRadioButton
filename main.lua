-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Load widget package.
local widget = require("widget")

-- Set background to beige.
display.setDefault("background", 245 / 255, 245 / 255, 220 / 255)

-- Init some location related parameters.
local radioLocX = 0.15
local radioLocY = 0.35
local radioOffset = 0.08

local itemLocX = 0.72
local itemLocY = 0.43

local textWidth = 150
local textLocX = 0.45

-- Init a Shape object.
local item = display.newCircle(display.contentWidth * itemLocX, display.contentHeight * itemLocY, 35)
item:setFillColor(255 / 255, 0 / 255, 0 / 255)

-- Declare the listener for radio Switch object.
local function onSwitchPress( event )
  local switch = event.target

  if switch.id == "RadioButton1" then
    item:removeSelf()

    item = display.newCircle(display.contentWidth * itemLocX, display.contentHeight * itemLocY, 35)
    item:setFillColor(255 / 255, 0 / 255, 0 / 255)
  elseif switch.id == "RadioButton2" then
    item:removeSelf()

    item = display.newRoundedRect(display.contentWidth * itemLocX, display.contentHeight * itemLocY, 75, 50, 10)
    item:setFillColor(0 / 255, 255 / 255, 0 / 255)
  elseif switch.id == "RadioButton3" then
    item:removeSelf()

    item = display.newPolygon(display.contentWidth * itemLocX, display.contentHeight * itemLocY,
      {-30, -35, -30, 35, 40, 0})
    item:setFillColor(0 / 255, 0 / 255, 255 / 255)
  end
end

-- Init a Group object for radio switches.
local radioGroup = display.newGroup()

-- Init the first radio Switch object.
local radioBtn1 = widget.newSwitch(
  {
    x = display.contentWidth * radioLocX,
    y = display.contentHeight * (radioLocY+ radioOffset * 0),
    id = "RadioButton1",
    style = "radio",
    initialSwitchState = true,
    onPress = onSwitchPress
  }
)

-- Insert the first object into the group.
radioGroup:insert(radioBtn1)

-- Init the text object for the first radio Switch object.
local btnText1 = display.newText(
  {
    text = "Circle",
    x = display.contentWidth * textLocX,
    y = display.contentHeight * (radioLocY + radioOffset * 0),
    fontSize = 20,
    width = textWidth,
  }
)

btnText1:setFillColor({ 0, 0, 0 })

-- Init the second radio Switch object.
local radioBtn2 = widget.newSwitch(
  {
    x = display.contentWidth * radioLocX,
    y = display.contentHeight * (radioLocY + radioOffset * 1),
    id = "RadioButton2",
    style = "radio",
    onPress = onSwitchPress
  }
)

-- Insert the second object into the group.
radioGroup:insert(radioBtn2)

-- Init the text object for the second radio Switch object.
local btnText2 = display.newText(
  {
    text = "Rectangle",
    x = display.contentWidth * textLocX,
    y = display.contentHeight * (radioLocY + radioOffset * 1),
    fontSize = 20,
    width = textWidth,
  }
)

btnText2:setFillColor({ 0, 0, 0 })

-- Init the third radio Switch object.
local radioBtn3 = widget.newSwitch(
  {
    x = display.contentWidth * radioLocX,
    y = display.contentHeight * (radioLocY + radioOffset * 2),
    id = "RadioButton3",
    style = "radio",
    onPress = onSwitchPress
  }
)

radioGroup:insert(radioBtn3)

-- Init the text object for the third radio Switch object.
local btnText3 = display.newText(
  {
    text = "Triangle",
    x = display.contentWidth * textLocX,
    y = display.contentHeight * (radioLocY + radioOffset * 2),
    fontSize = 20,
    width = textWidth,
  }
)

btnText3:setFillColor({ 0, 0, 0 })

-- Declare the holder of the checkbox Switch object.
local checkBtn

-- Declare the listener of the checkbox Switch object.
local function onCheckboxPress( event )
  local switch = event.target

  if switch.isOn then
    native.showAlert("Checkbox Event", "Checkmate", { "OK" },
      function ( ev )
        if ev.action == "clicked" then
          checkBtn:setState({isOn = false})
        end
      end)
  end
end

-- Init the checkbox Switch object.
checkBtn = widget.newSwitch(
  {
    x = display.contentWidth * radioLocX,
    y = display.contentHeight * (radioLocY + radioOffset * 3 + 0.03),
    style = "checkbox",
    onPress = onCheckboxPress,
  }
)

-- Init the text object for the checkbox object.
local checkboxText = display.newText(
  {
    text = "Check Me",
    x = display.contentWidth * textLocX,
    y = display.contentHeight * (radioLocY + radioOffset * 3 + 0.03),
    fontSize = 20,
    width = textWidth,
  }
)

checkboxText:setFillColor(0, 0, 0)
